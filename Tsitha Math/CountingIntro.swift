import SpriteKit
import GameplayKit

class CountingIntro: SKScene {
    var menuActive = true
    private var activecorn = 0
    
//    var numberLabel = SKLabelNode()
    
//    private var label : SKLabelNode?
    var englishLabel1 = SKLabelNode()
    var englishLabelTitle = SKLabelNode()

    let menubg = SKSpriteNode(imageNamed: "iconmenubg")
    let btncount = SKSpriteNode(imageNamed: "menucount")
    let btnadd = SKSpriteNode(imageNamed: "menuadd")
    let btnsub = SKSpriteNode(imageNamed: "menusub")
    let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
    let iconmenu = SKSpriteNode(imageNamed: "iconmenu")
    
    var bgcomplete = SKSpriteNode()
    var award = SKSpriteNode()
    var nextbutton = SKSpriteNode()
    
    let englishbutton1 = SKSpriteNode(imageNamed: "btnenglish2")
    let englishbutton2 = SKSpriteNode(imageNamed: "btnenglish2")

    var audionames = ["Zero",
                      "One",
                      "Two",
                      "Three",
                      "Four",
                      "Five",
                      "Six",
                      "Seven",
                      "Eight",
                      "Nine",
                      "Ten",
                      "Eleven",
                      "Twelve",
                      "Thirteen",
                      "Fourteen",
                      "Fifteen",
                      "Sixteen",
                      "Seventeen",
                      "Eighteen",
                      "Nineteen",
                      "Twenty"]
    
    override func didMove(to view: SKView) {
//        self.numberLabel = self.childNode(withName: "numberLabel") as! SKLabelNode
//        self.numberLabel.text = "\(activecorn)"

        self.bgcomplete = self.childNode(withName: "bgcomplete") as! SKSpriteNode
        self.award = self.childNode(withName: "award") as! SKSpriteNode
        self.nextbutton = self.childNode(withName: "nextbutton") as! SKSpriteNode
        self.award.isHidden = true
        self.bgcomplete.isHidden = true
        self.nextbutton.isHidden = true
        
//        nextbutton.isHidden = true
//        bgcomplete.isHidden = true
//        award.isHidden = true

        self.englishLabel1 = self.childNode(withName: "englishLabel1") as! SKLabelNode
        self.englishLabelTitle = self.childNode(withName: "englishLabelTitle") as! SKLabelNode

        englishLabel1.isHidden = true
        englishLabelTitle.isHidden = true
        
        // Menu background
        menubg.name = "menubackground"
        menubg.zPosition = -2
        menubg.position = CGPoint(x: self.size.width/5.06, y: self.size.height/1.09)
        menubg.size = CGSize(width: self.size.width/2.78, height: self.size.height/11.4)
        menubg.isHidden = true
        self.addChild(menubg)
        
        iconmenu.name = "iconmenu"
        iconmenu.zPosition = -1
        iconmenu.position = CGPoint(x: self.size.width/18, y: self.size.height/1.09)
        iconmenu.size = CGSize(width: 80, height: 80)
        iconmenu.isHidden = false
        self.addChild(iconmenu)
        
        btnhome.name = "btnhome"
        btnhome.zPosition = -1
        btnhome.position = CGPoint(x: self.size.width/7.8, y: self.size.height/1.09)
        btnhome.size = CGSize(width: 80, height: 80)
        btnhome.isHidden = true
        self.addChild(btnhome)
        
        btncount.name = "btncount"
        btncount.zPosition = -1
        btncount.position = CGPoint(x: self.size.width/5.2, y: self.size.height/1.09)
        btncount.size = CGSize(width: 80, height: 80)
        btncount.isHidden = true
        self.addChild(btncount)
        
        btnadd.name = "btnadd"
        btnadd.zPosition = -1
        btnadd.position = CGPoint(x: self.size.width/3.84, y: self.size.height/1.09)
        btnadd.size = CGSize(width: 105, height: 80)
        btnadd.isHidden = true
        self.addChild(btnadd)
        
        btnsub.name = "btnsub"
        btnsub.zPosition = -1
        btnsub.position = CGPoint(x: self.size.width/3, y: self.size.height/1.09)
        btnsub.size = CGSize(width: 98, height: 80)
        btnsub.isHidden = true
        self.addChild(btnsub)
        // End Menu
    }
    
    func playSound() {
        let otherWait = SKAction.wait(forDuration: 1)
        let runsound = SKAction.playSoundFileNamed("\(String(audionames[(activecorn)])).mp3", waitForCompletion: true)
        let otherSequence = SKAction.sequence([otherWait, runsound])
        run(otherSequence)
    }

    func playChime() {
        let otherWait = SKAction.wait(forDuration: 1)
        let runsound = SKAction.playSoundFileNamed("chime.mp3", waitForCompletion: true)
        let otherSequence = SKAction.sequence([otherWait, runsound])
        run(otherSequence)
        
    }
    
    func complete() {

        let completewait1 = SKAction.wait(forDuration: 0.4)
        let showbackground = SKAction.run {
            print("Completed this question: \(self.activecorn)")
            self.bgcomplete.isHidden = false
        }
            let showaward = SKAction.run {
            self.award.isHidden = false
        }
        let shownext = SKAction.run {
            self.nextbutton.isHidden = false
        }
        let setpositions = SKAction.run {
            self.bgcomplete.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
            self.bgcomplete.size = CGSize(width: self.size.width, height: self.size.height)
            self.award.position = CGPoint(x: self.size.width/2, y: self.size.height/1.6)
            self.nextbutton.position = CGPoint(x: self.size.width/2, y: self.size.height/4.3)

        }
        
            let completewait2 = SKAction.wait(forDuration: 0.2)
            let runsound = SKAction.playSoundFileNamed("chime.mp3", waitForCompletion: true)

        let otherSequence = SKAction.sequence([completewait1, showbackground, completewait1, showaward, completewait1, shownext, setpositions, completewait2, runsound])
        run(otherSequence)
    }
    
    func menuTouched() {
        if menuActive { // If menu is active
            btnsub.isHidden = false
            btnadd.isHidden = false
            btncount.isHidden = false
            btnhome.isHidden = false
            menubg.isHidden = false
            
            menuActive = false
        }
        else { // If menu is dormant
            btnsub.isHidden = true
            btnadd.isHidden = true
            btncount.isHidden = true
            btnhome.isHidden = true
            menubg.isHidden = true
            
            menuActive = true
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesarray = nodes(at: location)
            
            for node in nodesarray {

                if node.name == "corn\(activecorn)" {

                    if activecorn == 21 {
                        complete()
                    }
                    else {
                        print("Corn touched")
//                        self.numberLabel.text = "\(activecorn)"
                        activecorn = activecorn + 1
                        playSound()
                    }
                }
                
                if node.name == "iconmenu" {
                    menuTouched()
                }

                if node.name == "englishbutton1" {
                    englishLabel1.isHidden = false
                    englishLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                         SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                         SKAction.fadeOut(withDuration: 1)]))
                }
                
                if node.name == "englishbutton2" {
                    englishLabelTitle.isHidden = false
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                         SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                         SKAction.fadeOut(withDuration: 1)]))
                }
                
                if node.name == "btnhome" {
                    let meetScene = MeetTsitha(fileNamed: "GameScene")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "btncount" {
                    let meetScene = MeetTsitha(fileNamed: "CountingIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnadd" {
                    let meetScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnsub" {
                    let meetScene = MeetTsitha(fileNamed: "SubtractionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "btnnextwhite" {
                    let meetScene = MeetTsitha(fileNamed: "CountingScene1")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "nextbutton" {
                    let meetScene = MeetTsitha(fileNamed: "CountingScene1")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                
            }
            

        }
    }
}

