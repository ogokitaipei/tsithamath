import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    
    override func didMove(to view: SKView) {
//        backgroundColor = SKColor(red: 0.15, green:0.15, blue:0.3, alpha: 1.0)
//        let button = SKSpriteNode(imageNamed: "nextButton.png")
//        button.position = CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2)
//        button.name = "nextButton"
//
//        self.addChild(button)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesarray = nodes(at: location)
            
            for node in nodesarray {
                if node.name == "btnMeet" {
                    let meetScene = MeetTsitha(fileNamed: "MeetTsitha")
                    let transition = SKTransition.fade(withDuration: 0.5)
//                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnCounting" {
                    let meetScene = MeetTsitha(fileNamed: "CountingIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnAddition" {
                    let meetScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnSubtraction" {
                    let meetScene = MeetTsitha(fileNamed: "SubtractionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
            }
        }
    }
    
    
}
