import SpriteKit
import GameplayKit

class AddingTo5: SKScene {
    
    var value1 = 0
    var value2 = 10
    var checkcorrect = 1000
    var drag1active = true
    var drag2active = false
    var gameactive = true

    var question = 5

    var firstnumber = 1000
    var secondnumber = 1000
    
    let menubg = SKSpriteNode(imageNamed: "iconmenubg")
    let btncount = SKSpriteNode(imageNamed: "menucount")
    let btnadd = SKSpriteNode(imageNamed: "menuadd")
    let btnsub = SKSpriteNode(imageNamed: "menusub")
    let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
    let iconmenu = SKSpriteNode(imageNamed: "iconmenu")

    var bgcomplete = SKSpriteNode()
    var award = SKSpriteNode()
    var nextbutton = SKSpriteNode()
    
    var cornzero = SKSpriteNode()
    var cornone = SKSpriteNode()
    var corntwo = SKSpriteNode()
    var cornthree = SKSpriteNode()
    var cornfour = SKSpriteNode()
    var cornfive = SKSpriteNode()

    var engLabel0 = SKLabelNode()
    var engLabel1 = SKLabelNode()
    var engLabel2 = SKLabelNode()
    var engLabel3 = SKLabelNode()
    var engLabel4 = SKLabelNode()
    var engLabel5 = SKLabelNode()
    var engLabelAddition = SKLabelNode()
    var addLabel1 = SKLabelNode()
    var addLabel2 = SKLabelNode()
    var addLabel3 = SKLabelNode()
    var addLabel4 = SKLabelNode()
    var addLabel5 = SKLabelNode()

    var englishbutton0 = SKSpriteNode()
    var englishbutton1 = SKSpriteNode()
    var englishbutton2 = SKSpriteNode()
    var englishbutton3 = SKSpriteNode()
    var englishbutton4 = SKSpriteNode()
    var englishbutton5 = SKSpriteNode()
    var englishbuttonAdd = SKSpriteNode()

    var basketfamily1 = SKSpriteNode()
    var basketfamily2 = SKSpriteNode()
    var basketfamily3 = SKSpriteNode()
    var basketfamily4 = SKSpriteNode()
    var basketfamily5 = SKSpriteNode()
    
    
    override func didMove(to view: SKView) {
        
        // Menu background
        menubg.name = "menubackground"
        menubg.zPosition = -2
        menubg.position = CGPoint(x: self.size.width/5.06, y: self.size.height/1.09)
        menubg.size = CGSize(width: self.size.width/2.78, height: self.size.height/11.4)
        menubg.isHidden = true
        self.addChild(menubg)
        
        
        iconmenu.name = "iconmenu"
        iconmenu.zPosition = -1
        iconmenu.position = CGPoint(x: self.size.width/18, y: self.size.height/1.09)
        iconmenu.size = CGSize(width: 80, height: 80)
        iconmenu.isHidden = false
        self.addChild(iconmenu)
        //         let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
        
        btnhome.name = "btnhome"
        btnhome.zPosition = -1
        btnhome.position = CGPoint(x: self.size.width/7.8, y: self.size.height/1.09)
        btnhome.size = CGSize(width: 80, height: 80)
        btnhome.isHidden = true
        self.addChild(btnhome)
        
        btncount.name = "btncount"
        btncount.zPosition = -1
        btncount.position = CGPoint(x: self.size.width/5.2, y: self.size.height/1.09)
        btncount.size = CGSize(width: 80, height: 80)
        btncount.isHidden = true
        self.addChild(btncount)
        
        btnadd.name = "btnadd"
        btnadd.zPosition = -1
        btnadd.position = CGPoint(x: self.size.width/3.84, y: self.size.height/1.09)
        btnadd.size = CGSize(width: 105, height: 80)
        btnadd.isHidden = true
        self.addChild(btnadd)
        
        btnsub.name = "btnsub"
        btnsub.zPosition = -1
        btnsub.position = CGPoint(x: self.size.width/3, y: self.size.height/1.09)
        btnsub.size = CGSize(width: 98, height: 80)
        btnsub.isHidden = true
        self.addChild(btnsub)
        // End Menu
        
    
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        self.cornzero = self.childNode(withName: "cornzero") as! SKSpriteNode
        self.cornone = self.childNode(withName: "cornone") as! SKSpriteNode
        self.corntwo = self.childNode(withName: "corntwo") as! SKSpriteNode
        self.cornthree = self.childNode(withName: "cornthree") as! SKSpriteNode
        self.cornfour = self.childNode(withName: "cornfour") as! SKSpriteNode
        self.cornfive = self.childNode(withName: "cornfive") as! SKSpriteNode

        self.engLabel0 = self.childNode(withName: "engLabel0") as! SKLabelNode
        self.engLabel1 = self.childNode(withName: "engLabel1") as! SKLabelNode
        self.engLabel2 = self.childNode(withName: "engLabel2") as! SKLabelNode
        self.engLabel3 = self.childNode(withName: "engLabel3") as! SKLabelNode
        self.engLabel4 = self.childNode(withName: "engLabel4") as! SKLabelNode
        self.engLabel5 = self.childNode(withName: "engLabel5") as! SKLabelNode
        self.engLabelAddition = self.childNode(withName: "engLabelAddition") as! SKLabelNode
        self.addLabel1 = self.childNode(withName: "addLabel1") as! SKLabelNode
        self.addLabel2 = self.childNode(withName: "addLabel2") as! SKLabelNode
        self.addLabel3 = self.childNode(withName: "addLabel3") as! SKLabelNode
        self.addLabel4 = self.childNode(withName: "addLabel4") as! SKLabelNode
        self.addLabel5 = self.childNode(withName: "addLabel5") as! SKLabelNode

        self.englishbutton0 = self.childNode(withName: "englishbutton0") as! SKSpriteNode
        self.englishbutton1 = self.childNode(withName: "englishbutton1") as! SKSpriteNode
        self.englishbutton2 = self.childNode(withName: "englishbutton2") as! SKSpriteNode
        self.englishbutton3 = self.childNode(withName: "englishbutton3") as! SKSpriteNode
        self.englishbutton4 = self.childNode(withName: "englishbutton4") as! SKSpriteNode
        self.englishbutton5 = self.childNode(withName: "englishbutton5") as! SKSpriteNode
        self.englishbuttonAdd = self.childNode(withName: "englishbuttonAdd") as! SKSpriteNode

        self.basketfamily1 = self.childNode(withName: "basketfamily1") as! SKSpriteNode
        self.basketfamily2 = self.childNode(withName: "basketfamily2") as! SKSpriteNode
        self.basketfamily3 = self.childNode(withName: "basketfamily3") as! SKSpriteNode
        self.basketfamily4 = self.childNode(withName: "basketfamily4") as! SKSpriteNode
        self.basketfamily5 = self.childNode(withName: "basketfamily5") as! SKSpriteNode

        self.bgcomplete = self.childNode(withName: "bgcomplete") as! SKSpriteNode
        self.award = self.childNode(withName: "award") as! SKSpriteNode
        self.nextbutton = self.childNode(withName: "nextbutton") as! SKSpriteNode
        self.award.isHidden = true
        self.bgcomplete.isHidden = true
        self.nextbutton.isHidden = true
        
        addLabel1.text = " _ + _ = 1"
        addLabel2.text = " _ + _ = 2"
        addLabel3.text = " _ + _ = 3"
        addLabel4.text = " _ + _ = 4"
        addLabel5.text = " _ + _ = 5"
        
        setupcorn()

    }
    
    func setupcorn() {

        nextbutton.isHidden = true
        bgcomplete.isHidden = true
        award.isHidden = true
        drag2active = false
        drag1active = true


        cornzero.position = CGPoint(x: self.size.width/10, y: self.size.height/1.4)
        cornone.position = CGPoint(x: self.size.width/4.2, y: self.size.height/1.4)
        corntwo.position = CGPoint(x: self.size.width/2.5, y: self.size.height/1.4)
        cornthree.position = CGPoint(x: self.size.width/1.8, y: self.size.height/1.4)
        cornfour.position = CGPoint(x: self.size.width/1.38, y: self.size.height/1.4)
        cornfive.position = CGPoint(x: self.size.width/1.1, y: self.size.height/1.4)

        let basketlooploop = SKAction.repeatForever(SKAction.init(named: "baskets")!)
        
        if question == 5 {
            basketfamily5.run(basketlooploop, withKey:"loop")
        }
        if question == 4 {
            basketfamily4.run(basketlooploop, withKey:"loop")
            basketfamily5.removeAction(forKey: "loop")
        }
        if question == 3 {
            basketfamily3.run(basketlooploop, withKey:"loop")
            basketfamily4.removeAction(forKey: "loop")
        }
        if question == 2 {
            basketfamily2.run(basketlooploop, withKey:"loop")
            basketfamily3.removeAction(forKey: "loop")
        }
        if question == 1 {
            basketfamily1.run(basketlooploop, withKey:"loop")
            basketfamily2.removeAction(forKey: "loop")
        }
        if question == 0 {
            basketfamily1.removeAction(forKey: "loop")
        }

    }
    
    func complete() {
        print("Completed this question: \(question)")
        
        question = question - 1

        drag2active = false
        drag1active = false
        gameactive = true

        bgcomplete.isHidden = false
        award.isHidden = false
        nextbutton.isHidden = false
        
        bgcomplete.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        bgcomplete.size = CGSize(width: self.size.width, height: self.size.height)
        award.position = CGPoint(x: self.size.width/2, y: self.size.height/1.6)
        nextbutton.position = CGPoint(x: self.size.width/2, y: self.size.height/4.3)

        print("We are on question number \(question)")
        

    }
    
    func fullcomplete() {
        print("You Win!!!!!")
        
    }
    
    func lockactive1()  {

        if question == 5 {
            if drag2active == false {
                addLabel5.text = " \(value1) + _ = 5"
                drag2active = true
                }
        else {
            checkcorrect = value1 + value2
        }
        if checkcorrect == 5 {
            addLabel5.text = " \(value1) + \(value2) = 5"
            print("YOU WIN")
            gameactive = false
            complete()
            }
        }
        
         if question == 4 {
            if drag1active == false {
                addLabel4.text = " \(value1) + _ = 4"
                drag2active = true
            }
            else {
                checkcorrect = value1 + value2
            }
            if checkcorrect == 4 {
                addLabel4.text = " \(value1) + \(value2) = 4"
                print("YOU WIN")
                gameactive = false
                complete()
            }
        }
         if question == 3 {
            if drag2active == false {
                addLabel3.text = " \(value1) + _ = 3"
                drag2active = true
            }
            else {
                checkcorrect = value1 + value2
            }
            if checkcorrect == 3 {
                addLabel3.text = " \(value1) + \(value2) = 3"
                print("YOU WIN")
                gameactive = false
                complete()
            }
        }
        
         if question == 2 {
            if drag2active == false {
                addLabel2.text = " \(value1) + _ = 2"
                drag2active = true
            }
            else {
                checkcorrect = value1 + value2
            }
            if checkcorrect == 2 {
                addLabel2.text = " \(value1) + \(value2) = 2"
                print("YOU WIN")
                gameactive = false
                complete()
            }
        }
        
         if question == 1 {
            if drag2active == false {
                addLabel1.text = " \(value1) + _ = 1"
                drag2active = true
            }
            else {
                checkcorrect = value1 + value2
            }
            if checkcorrect == 1 {
                addLabel1.text = " \(value1) + \(value2) = 1"
                print("YOU WIN")
                gameactive = false
                complete()
            }
        }
        
         if question == 0 {
            fullcomplete()
        }
        
        
    }

    
    func lockactive2() {
        
        cornzero.position = CGPoint(x: self.size.width/10, y: self.size.height/1.4)
        cornone.position = CGPoint(x: self.size.width/4.2, y: self.size.height/1.4)
        corntwo.position = CGPoint(x: self.size.width/2.5, y: self.size.height/1.4)
        cornthree.position = CGPoint(x: self.size.width/1.8, y: self.size.height/1.4)
        cornfour.position = CGPoint(x: self.size.width/1.38, y: self.size.height/1.4)
        cornfive.position = CGPoint(x: self.size.width/1.1, y: self.size.height/1.4)
        
    }
    
    func touchDown(atPoint pos : CGPoint) {

    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {

        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
        let touch = touches.first!
        
        let location = touch.location(in: self)
        let nodesarray = nodes(at: location)

        for node in nodesarray {
            
            if node.name == "btnnextwhite" {
                let meetScene = MeetTsitha(fileNamed: "AddingTo10")
                let transition = SKTransition.fade(withDuration: 0.5)
                //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                meetScene?.scaleMode = .aspectFill
                scene?.view?.presentScene(meetScene!, transition: transition)
            }
            
        if nextbutton.frame.contains(touch.location(in: self)){
            setupcorn()
        }

        if gameactive == true {
            
            if cornzero.frame.contains(touch.previousLocation(in: self)){
                if drag2active == false {
                    drag1active = true
                value1 = 0
                }
                else {
                    drag1active = false
                    value2 = 0
                }
        }

            if cornone.frame.contains(touch.previousLocation(in: self)){
                if drag2active == false {
                    drag1active = true
                value1 = 1
                }
                    
                if drag2active == true {
                    drag1active = false
                    value2 = 1
                }
        }
            if corntwo.frame.contains(touch.previousLocation(in: self)){
                if drag2active == false {
                value1 = 2
                }
                if drag2active == true {
                    value2 = 2
                }
        }
                if cornthree.frame.contains(touch.previousLocation(in: self)){
                    if drag2active == false {
                value1 = 3
                }
                    if drag2active == true {
                    value2 = 3
                    }
        }
            if cornfour.frame.contains(touch.previousLocation(in: self)){
                if drag2active == false {
                value1 = 4
                }
                if drag2active == true {
                    value2 = 4
                }
        }
            if cornfive.frame.contains(touch.previousLocation(in: self)){
                if drag2active == false {
                    drag1active = true

                value1 = 5
            }
                if drag2active == true {
                    drag1active = false
                    value2 = 5
                }
        }
            
        }
            
        }
        
        if englishbutton0.frame.contains(touch.previousLocation(in: self)){
            engLabel0.isHidden = false
            engLabel0.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                             SKAction.fadeIn(withDuration: 0.5)]))
            engLabel0.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                   SKAction.fadeOut(withDuration: 1)]))
        }
        
        if englishbutton1.frame.contains(touch.previousLocation(in: self)){
            engLabel1.isHidden = false
            engLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                             SKAction.fadeIn(withDuration: 0.5)]))
            engLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                             SKAction.fadeOut(withDuration: 1)]))
        }

        if englishbutton2.frame.contains(touch.previousLocation(in: self)){
            engLabel2.isHidden = false
            engLabel2.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                             SKAction.fadeIn(withDuration: 0.5)]))
            engLabel2.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                             SKAction.fadeOut(withDuration: 1)]))
        }

        if englishbutton3.frame.contains(touch.previousLocation(in: self)){
            engLabel3.isHidden = false
            engLabel3.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                             SKAction.fadeIn(withDuration: 0.5)]))
            engLabel3.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                             SKAction.fadeOut(withDuration: 1)]))
        }

        if englishbutton4.frame.contains(touch.previousLocation(in: self)){
            engLabel4.isHidden = false
            engLabel4.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                             SKAction.fadeIn(withDuration: 0.5)]))
            engLabel4.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                             SKAction.fadeOut(withDuration: 1)]))
        }

        if englishbutton5.frame.contains(touch.previousLocation(in: self)){
            engLabel5.isHidden = false
            engLabel5.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                             SKAction.fadeIn(withDuration: 0.5)]))
            engLabel5.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                             SKAction.fadeOut(withDuration: 1)]))
        }
        
        if englishbuttonAdd.frame.contains(touch.previousLocation(in: self)){
            engLabelAddition.isHidden = false
            engLabelAddition.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                             SKAction.fadeIn(withDuration: 0.5)]))
            engLabelAddition.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                             SKAction.fadeOut(withDuration: 1)]))
        }


  //      for t in touches { self.touchDown(atPoint: t.location(in: self)) }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
        let touch = touches.first!
        
        if gameactive == true {
            
            if cornzero.frame.contains(touch.previousLocation(in: self)){
                cornzero.position = touch.location(in: self)
        }
            if cornone.frame.contains(touch.previousLocation(in: self)){
                cornone.position = touch.location(in: self)
        }
            if corntwo.frame.contains(touch.previousLocation(in: self)){
                corntwo.position = touch.location(in: self)
        }
            if cornthree.frame.contains(touch.previousLocation(in: self)){
                cornthree.position = touch.location(in: self)
        }
             if cornfour.frame.contains(touch.previousLocation(in: self)){
                cornfour.position = touch.location(in: self)
        }
             if cornfive.frame.contains(touch.previousLocation(in: self)){
                cornfive.position = touch.location(in: self)
        }

    }
    }

    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
        
        if question == 5 {

        if basketfamily5.frame.contains(cornzero.position){
            
        lockactive1()
            cornzero.position = CGPoint(x: basketfamily5.position.x - 50, y:basketfamily5.position.y)
            cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
            cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/10, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
        }

        if basketfamily5.frame.contains(cornone.position){

        lockactive1()
        cornone.position = CGPoint(x: basketfamily5.position.x - 50, y:basketfamily5.position.y)
            cornone.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
            cornone.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/4.2, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
        }

        if basketfamily5.frame.contains(corntwo.position){
            
            lockactive1()
            corntwo.position = CGPoint(x: basketfamily5.position.x - 50, y:basketfamily5.position.y)
            corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
            corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/2.5, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))        }
        
        if basketfamily5.frame.contains(cornthree.position){
            
            lockactive1()
            cornthree.position = CGPoint(x: basketfamily5.position.x - 50, y:basketfamily5.position.y)
            cornthree.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
            cornthree.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/1.8, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
        }
        
        if basketfamily5.frame.contains(cornfour.position){
            
            lockactive1()
            cornfour.position = CGPoint(x: basketfamily5.position.x - 50, y:basketfamily5.position.y)
            cornfour.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
            cornfour.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/1.38, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
        }
        
        if basketfamily5.frame.contains(cornfive.position){
            
            lockactive1()
            cornfive.position = CGPoint(x: basketfamily5.position.x - 50, y:basketfamily5.position.y)
            cornfive.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
            cornfive.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/1.1, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            
        }
        }
        
        if question == 4 {
            
            if basketfamily4.frame.contains(cornzero.position){
                
                lockactive1()
                cornzero.position = CGPoint(x: basketfamily4.position.x - 50, y:basketfamily4.position.y)
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/10, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily4.frame.contains(cornone.position){
                
                lockactive1()
                cornone.position = CGPoint(x: basketfamily4.position.x - 50, y:basketfamily4.position.y)
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/4.2, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily4.frame.contains(corntwo.position){
                
                lockactive1()
                corntwo.position = CGPoint(x: basketfamily4.position.x - 50, y:basketfamily4.position.y)
                corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/2.5, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))        }
            
            if basketfamily4.frame.contains(cornthree.position){
                
                lockactive1()
                cornthree.position = CGPoint(x: basketfamily4.position.x - 50, y:basketfamily4.position.y)
                cornthree.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornthree.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/1.8, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily4.frame.contains(cornfour.position){
                
                lockactive1()
                cornfour.position = CGPoint(x: basketfamily4.position.x - 50, y:basketfamily4.position.y)
                cornfour.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornfour.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/1.38, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
        }
        
        if question == 3 {
            
            if basketfamily3.frame.contains(cornzero.position){
                
                lockactive1()
                cornzero.position = CGPoint(x: basketfamily3.position.x - 50, y:basketfamily3.position.y)
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/10, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily3.frame.contains(cornone.position){
                
                lockactive1()
                cornone.position = CGPoint(x: basketfamily3.position.x - 50, y:basketfamily3.position.y)
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/4.2, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily3.frame.contains(corntwo.position){
                
                lockactive1()
                corntwo.position = CGPoint(x: basketfamily3.position.x - 50, y:basketfamily3.position.y)
                corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/2.5, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))        }
            
            if basketfamily3.frame.contains(cornthree.position){
                
                lockactive1()
                cornthree.position = CGPoint(x: basketfamily3.position.x - 50, y:basketfamily3.position.y)
                cornthree.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornthree.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/1.8, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
        }
        
        if question == 2 {
            
            if basketfamily2.frame.contains(cornzero.position){
                
                lockactive1()
                cornzero.position = CGPoint(x: basketfamily2.position.x - 50, y:basketfamily2.position.y)
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/10, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily2.frame.contains(cornone.position){
                
                lockactive1()
                cornone.position = CGPoint(x: basketfamily2.position.x - 50, y:basketfamily2.position.y)
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/4.2, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily2.frame.contains(corntwo.position){
                
                lockactive1()
                corntwo.position = CGPoint(x: basketfamily2.position.x - 50, y:basketfamily2.position.y)
                corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                corntwo.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/2.5, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))        }
            
        }
        
        if question == 1 {
            
            if basketfamily1.frame.contains(cornzero.position){
                
                lockactive1()
                cornzero.position = CGPoint(x: basketfamily1.position.x - 50, y:basketfamily1.position.y)
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornzero.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/10, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
            
            if basketfamily1.frame.contains(cornone.position){
                
                lockactive1()
                cornone.position = CGPoint(x: basketfamily1.position.x - 50, y:basketfamily1.position.y)
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 1.0), SKAction.fadeOut(withDuration: 1)]))
                cornone.run(SKAction.sequence([SKAction.wait(forDuration: 2.5), SKAction.moveTo(x: self.size.width/4.2, duration: 0), SKAction.moveTo(y: self.size.height/1.4, duration: 0), SKAction.fadeIn(withDuration: 0.5),]))
            }
        }
    }
    
    
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
