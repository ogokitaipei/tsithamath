import SpriteKit
import GameplayKit

class AdditionIntro: SKScene {
    var menuActive = true

    var englishLabel1 = SKLabelNode()
    var englishLabelTitle = SKLabelNode()
    
    let menubg = SKSpriteNode(imageNamed: "iconmenubg")
    let btncount = SKSpriteNode(imageNamed: "menucount")
    let btnadd = SKSpriteNode(imageNamed: "menuadd")
    let btnsub = SKSpriteNode(imageNamed: "menusub")
    let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
    let iconmenu = SKSpriteNode(imageNamed: "iconmenu")
    
    let englishbutton1 = SKSpriteNode(imageNamed: "btnenglish2")
    let englishbutton2 = SKSpriteNode(imageNamed: "btnenglish2")

    override func didMove(to view: SKView) {

        self.englishLabel1 = self.childNode(withName: "englishLabel1") as! SKLabelNode
        self.englishLabelTitle = self.childNode(withName: "englishLabelTitle") as! SKLabelNode

        englishLabel1.isHidden = true
        englishLabelTitle.isHidden = true

        // Menu background
        menubg.name = "menubackground"
        menubg.zPosition = -2
        menubg.position = CGPoint(x: self.size.width/5.06, y: self.size.height/1.09)
        menubg.size = CGSize(width: self.size.width/2.78, height: self.size.height/11.4)
        menubg.isHidden = true
        self.addChild(menubg)
        
        
        iconmenu.name = "iconmenu"
        iconmenu.zPosition = -1
        iconmenu.position = CGPoint(x: self.size.width/18, y: self.size.height/1.09)
        iconmenu.size = CGSize(width: 80, height: 80)
        iconmenu.isHidden = false
        self.addChild(iconmenu)
        //         let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
        
        btnhome.name = "btnhome"
        btnhome.zPosition = -1
        btnhome.position = CGPoint(x: self.size.width/7.8, y: self.size.height/1.09)
        btnhome.size = CGSize(width: 80, height: 80)
        btnhome.isHidden = true
        self.addChild(btnhome)
        
        btncount.name = "btncount"
        btncount.zPosition = -1
        btncount.position = CGPoint(x: self.size.width/5.2, y: self.size.height/1.09)
        btncount.size = CGSize(width: 80, height: 80)
        btncount.isHidden = true
        self.addChild(btncount)
        
        btnadd.name = "btnadd"
        btnadd.zPosition = -1
        btnadd.position = CGPoint(x: self.size.width/3.84, y: self.size.height/1.09)
        btnadd.size = CGSize(width: 105, height: 80)
        btnadd.isHidden = true
        self.addChild(btnadd)
        
        btnsub.name = "btnsub"
        btnsub.zPosition = -1
        btnsub.position = CGPoint(x: self.size.width/3, y: self.size.height/1.09)
        btnsub.size = CGSize(width: 98, height: 80)
        btnsub.isHidden = true
        self.addChild(btnsub)
        // End Menu
        
    }
    
    func menuTouched() {
        if menuActive { // If menu is active
            btnsub.isHidden = false
            btnadd.isHidden = false
            btncount.isHidden = false
            btnhome.isHidden = false
            menubg.isHidden = false
            
            menuActive = false
        }
        else { // If menu is dormant
            btnsub.isHidden = true
            btnadd.isHidden = true
            btncount.isHidden = true
            btnhome.isHidden = true
            menubg.isHidden = true
            
            menuActive = true
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesarray = nodes(at: location)

            for node in nodesarray {
                
                if node.name == "iconmenu" {
                    menuTouched()
                }
                
                if node.name == "englishbutton1" {
                    englishLabel1.isHidden = false
                    englishLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                         SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                         SKAction.fadeOut(withDuration: 1)]))
                }
                
                if node.name == "englishbutton2" {
                    englishLabelTitle.isHidden = false
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                             SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                             SKAction.fadeOut(withDuration: 1)]))
                }
   
                if node.name == "btnhome" {
                    let meetScene = MeetTsitha(fileNamed: "GameScene")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "btncount" {
                    let meetScene = MeetTsitha(fileNamed: "CountingIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnadd" {
                    let meetScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnsub" {
                    let meetScene = MeetTsitha(fileNamed: "SubtractionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "btnnextwhite" {
                    let meetScene = MeetTsitha(fileNamed: "AddingTo52")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                
            }
            
        }
    }
    
}
