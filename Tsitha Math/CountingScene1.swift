import SpriteKit
import GameplayKit

class CountingScene1: SKScene {
    var menuActive = true
    var gameActive = true
    
    private var currentNode: SKNode?
    var numberLabel = SKLabelNode()
    var spellingLabel = SKLabelNode()
    
    private var activecorn = 0
    private var activebasket = 5
    private var mohawkWord = "Lathe:non"
    
    var lastPoint:CGPoint?

    var englishLabelTitle = SKLabelNode()

    let menubg = SKSpriteNode(imageNamed: "iconmenubg")
    let btncount = SKSpriteNode(imageNamed: "menucount")
    let btnadd = SKSpriteNode(imageNamed: "menuadd")
    let btnsub = SKSpriteNode(imageNamed: "menusub")
    let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
    let iconmenu = SKSpriteNode(imageNamed: "iconmenu")
    var basketfull1 = SKSpriteNode()
    var basketfull2 = SKSpriteNode()
    var basketfull3 = SKSpriteNode()
    var basketfull4 = SKSpriteNode()
    var basketfull5 = SKSpriteNode()

    var bgcomplete = SKSpriteNode()
    var award = SKSpriteNode()
    var nextbutton = SKSpriteNode()
    
    let englishbutton2 = SKSpriteNode(imageNamed: "btnenglish2")

    var emptybasket = SKSpriteNode()
    var selectNode = SKSpriteNode()
    
    var audionames = ["Zero",
                      "One",
                      "Two",
                      "Three",
                      "Four",
                      "Five",
                      "Six",
                      "Seven",
                      "Eight",
                      "Nine",
                      "Ten",
                      "Eleven",
                      "Twelve",
                      "Thirteen",
                      "Fourteen",
                      "Fifteen",
                      "Sixteen",
                      "Seventeen",
                      "Eighteen",
                      "Nineteen",
                      "Twenty"]
    
    var spellingnames = ["Lathe:non",
                        "Énska",
                        "Tékeni",
                        "Áhsen",
                        "Kayé:ri",
                        "Wísk",
                        "Yá:ya’k",
                        "Tsyá:ta",
                        "Sha’té:kon",
                        "Tyóhton",
                        "Oyé:ri",
                        "Énskayawén:re",
                        "Tékeniyawén:re",
                        "Áhsenyawén:re",
                        "Kayé:riyawén:re",
                        "Wískyawén:re",
                        "Yá:ya’kyawén:re",
                        "Tsyá:tayawén:re",
                        "Sha’té:konyawén:re",
                        "Tyóhtonyawén:re",
                        "Tewáhsen"]
    
    let imageNames = ["corn0",
                      "corn1",
                      "corn2",
                      "corn3",
                      "corn4",
                      "corn5",
                      "corn6",
                      "corn7",
                      "corn8",
                      "corn9",
                      "corn10",
                      "corn11",
                      "corn12",
                      "corn13",
                      "corn14",
                      "corn15",
                      "corn16",
                      "corn17",
                      "corn18",
                      "corn19",
                      "corn20",
    ]
    
    override func didMove(to view: SKView) {
        self.numberLabel = self.childNode(withName: "numberLabel") as! SKLabelNode
        self.spellingLabel = self.childNode(withName: "spellingLabel") as! SKLabelNode
        self.numberLabel.text = "\(activecorn)"
        self.spellingLabel.text = "\(mohawkWord)"

//        self.basketfull1 = self.childNode(withName: "basketfull1") as! SKSpriteNode
//        self.basketfull2 = self.childNode(withName: "basketfull2") as! SKSpriteNode
//        self.basketfull3 = self.childNode(withName: "basketfull3") as! SKSpriteNode
//        self.basketfull4 = self.childNode(withName: "basketfull4") as! SKSpriteNode
//        self.basketfull5 = self.childNode(withName: "basketfull5") as! SKSpriteNode

        self.bgcomplete = self.childNode(withName: "bgcomplete") as! SKSpriteNode
        self.award = self.childNode(withName: "award") as! SKSpriteNode
        self.nextbutton = self.childNode(withName: "nextbutton") as! SKSpriteNode
        self.award.isHidden = true
        self.bgcomplete.isHidden = true
        self.nextbutton.isHidden = true

        nextbutton.isHidden = true
        bgcomplete.isHidden = true
        award.isHidden = true
        
        self.englishLabelTitle = self.childNode(withName: "englishLabelTitle") as! SKLabelNode
        englishLabelTitle.isHidden = true

        self.emptybasket = self.childNode(withName: "emptybasket") as! SKSpriteNode
        emptybasket.zPosition = 1
        
        // Menu background
        menubg.name = "menubackground"
        menubg.zPosition = -2
        menubg.position = CGPoint(x: self.size.width/5.06, y: self.size.height/1.09)
        menubg.size = CGSize(width: self.size.width/2.78, height: self.size.height/11.4)
        menubg.isHidden = true
        self.addChild(menubg)
        
        iconmenu.name = "iconmenu"
        iconmenu.zPosition = -1
        iconmenu.position = CGPoint(x: self.size.width/18, y: self.size.height/1.09)
        iconmenu.size = CGSize(width: 80, height: 80)
        iconmenu.isHidden = false
        self.addChild(iconmenu)
        
        btnhome.name = "btnhome"
        btnhome.zPosition = -1
        btnhome.position = CGPoint(x: self.size.width/7.8, y: self.size.height/1.09)
        btnhome.size = CGSize(width: 80, height: 80)
        btnhome.isHidden = true
        self.addChild(btnhome)
        
        btncount.name = "btncount"
        btncount.zPosition = -1
        btncount.position = CGPoint(x: self.size.width/5.2, y: self.size.height/1.09)
        btncount.size = CGSize(width: 80, height: 80)
        btncount.isHidden = true
        self.addChild(btncount)
        
        btnadd.name = "btnadd"
        btnadd.zPosition = -1
        btnadd.position = CGPoint(x: self.size.width/3.84, y: self.size.height/1.09)
        btnadd.size = CGSize(width: 105, height: 80)
        btnadd.isHidden = true
        self.addChild(btnadd)
        
        btnsub.name = "btnsub"
        btnsub.zPosition = -1
        btnsub.position = CGPoint(x: self.size.width/3, y: self.size.height/1.09)
        btnsub.size = CGSize(width: 98, height: 80)
        btnsub.isHidden = true
        self.addChild(btnsub)
        // End Menu
        
        for i in 0..<imageNames.count {
            
            let imageName = imageNames[i]
            let sprite = SKSpriteNode(imageNamed: imageName)
            
            if i < 11 {
                let offsetFraction = (CGFloat(i) + 1.0)/(CGFloat(imageNames.count - 10) + 1.0)
                sprite.position = CGPoint(x: size.width * offsetFraction, y: size.height / 1.6)
            }
            else {
                let offsetFraction = (CGFloat(i) - 11 + 1.0)/(CGFloat(imageNames.count - 11) + 1.0)
                sprite.position = CGPoint(x: size.width * offsetFraction, y: size.height / 2.4)
            }
            sprite.zPosition = 12
            sprite.name = "draggable\(i)"
            addChild(sprite)
        }
        
    }
    
    func playSound() {
        let otherWait = SKAction.wait(forDuration: 1)
        let runsound = SKAction.playSoundFileNamed("\(String(audionames[(activecorn)])).mp3", waitForCompletion: true)
        let otherSequence = SKAction.sequence([otherWait, runsound])
        run(otherSequence)
    }
    
    func playChime() {
        let otherWait = SKAction.wait(forDuration: 1)
        let runsound = SKAction.playSoundFileNamed("chime.mp3", waitForCompletion: true)
        let otherSequence = SKAction.sequence([otherWait, runsound])
        run(otherSequence)
        
    }
    
    func addToMoreCorn() {
//        if activecorn == 1 {
////            basketfull1.run(SKAction.repeatForever(SKAction.rotate(byAngle: CGFloat(Double.pi), duration: 1)))
//            basketfull1.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
//                                              SKAction.fadeIn(withDuration: 0.5)]))
//        }
    }
    
    func nextNumber() {
        playChime()
        activecorn = activecorn + 1
        self.numberLabel.text = "\(activecorn)"
        self.spellingLabel.text = "\(String(spellingnames[(activecorn)]))"
        gameActive = false
    }
    
    func complete() {
        
        let completewait1 = SKAction.wait(forDuration: 0.4)
        let showbackground = SKAction.run {
            print("Completed this question: \(self.activecorn)")
            self.bgcomplete.isHidden = false
        }
        let showaward = SKAction.run {
            self.award.isHidden = false
        }
        let shownext = SKAction.run {
            self.nextbutton.isHidden = false
        }
        let setpositions = SKAction.run {
            self.bgcomplete.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
            self.bgcomplete.size = CGSize(width: self.size.width, height: self.size.height)
            self.award.position = CGPoint(x: self.size.width/2, y: self.size.height/1.6)
            self.nextbutton.position = CGPoint(x: self.size.width/2, y: self.size.height/4.3)
            
        }
        
        let completewait2 = SKAction.wait(forDuration: 0.2)
        let runsound = SKAction.playSoundFileNamed("chime.mp3", waitForCompletion: true)
        
        let otherSequence = SKAction.sequence([completewait1, showbackground, completewait1, showaward, completewait1, shownext, setpositions, completewait2, runsound])
        run(otherSequence)
    }
    
    func menuTouched() {
        if menuActive { // If menu is active
            btnsub.isHidden = false
            btnadd.isHidden = false
            btncount.isHidden = false
            btnhome.isHidden = false
            menubg.isHidden = false
            
            menuActive = false
        }
        else { // If menu is dormant
            btnsub.isHidden = true
            btnadd.isHidden = true
            btncount.isHidden = true
            btnhome.isHidden = true
            menubg.isHidden = true
            
            menuActive = true
        }
        
    }
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesarray = nodes(at: location)
            let btneng1 = nodes(at: location)
            
            for node in nodesarray {
                
                if node.name == "draggable\(activecorn)" {
                    
                    if activecorn == 21 {
                        complete()
                    }
                    else {
                        print("Corn touched")
//                        activecorn = activecorn + 1
                        playSound()
                    }
                }
                
                if node.name == "iconmenu" {
                    menuTouched()
                }
                
                
                if node.name == "englishbutton2" {
                    englishLabelTitle.isHidden = false
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                             SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                             SKAction.fadeOut(withDuration: 1)]))
                    
                }
                
                if node.name == "btnhome" {
                    let meetScene = MeetTsitha(fileNamed: "GameScene")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "btncount" {
                    let meetScene = MeetTsitha(fileNamed: "CountingIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnadd" {
                    let meetScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnsub" {
                    let meetScene = MeetTsitha(fileNamed: "SubtractionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "btnnextwhite" {
                    let meetScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
                if node.name == "nextbutton" {
                    let meetScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                
            }
            
            if let touch = touches.first {
                let location = touch.location(in: self)

                let touchedNodes = self.nodes(at: location)

                for node in touchedNodes.reversed() {
                    if node.name == "draggable\(activecorn)" {
                        self.currentNode = node
                        selectNodeForTouch()
                        gameActive = true
                    }
                }
                
            }
            
            for node in btneng1 {
                if node.name == "englishbutton1" {
                    
                }
            }
            
        }
    }
    
    func degToRad(degree: Double) -> CGFloat {
        return CGFloat(Double(degree) / 180.0 * Double.pi)
    }
    
    func selectNodeForTouch() {
        emptybasket.run(SKAction.rotate(toAngle: 0.0, duration: 0.1))
        let sequence = SKAction.sequence([SKAction.rotate(byAngle: degToRad(degree: -4.0), duration: 0.1),
                                          SKAction.rotate(byAngle: 0.0, duration: 0.1),
                                          SKAction.rotate(byAngle: degToRad(degree: 4.0), duration: 0.1)])
        emptybasket.run(SKAction.repeatForever(sequence))
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let node = currentNode {
            let touchLocation = touch.location(in: self)
            node.position = touchLocation
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
        
        emptybasket.removeAllActions()
        
        print(emptybasket.frame)
        
        guard let current = currentNode?.frame else { return }

        if emptybasket.frame.intersects(current){
            
                if activecorn == 20 {
                    complete()
                }

                
                else {
                    if gameActive == true {
                        currentNode?.removeFromParent()
                        gameActive = false
                        nextNumber()

                    }
                }

        }

    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.currentNode = nil
    }
    
    
}
