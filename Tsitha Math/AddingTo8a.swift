import SpriteKit
import GameplayKit

class AddingTo8a: SKScene {
    
    private var currentNode: SKNode?
    
    private var activecorn = 0
    private var activemove = true
    private var activegame = true
    private var placeone = 99
    private var placetwo = 88
    private var currentbasket = 8
    
    var numberLabel = SKLabelNode()
    var placeoneLabel = SKLabelNode()
    var placetwoLabel = SKLabelNode()
    var basket0 = SKSpriteNode()
    var basket1 = SKSpriteNode()
    var basket2 = SKSpriteNode()
    var basket3 = SKSpriteNode()
    var basket4 = SKSpriteNode()
    var basket5 = SKSpriteNode()
    var basket6 = SKSpriteNode()
    var basket7 = SKSpriteNode()
    var basket8 = SKSpriteNode()
    var basket9 = SKSpriteNode()
    var basket10 = SKSpriteNode()
    var draggable0 = SKSpriteNode()
    var draggable1 = SKSpriteNode()
    var draggable2 = SKSpriteNode()
    var draggable3 = SKSpriteNode()
    var draggable4 = SKSpriteNode()
    var draggable5 = SKSpriteNode()
    var draggable6 = SKSpriteNode()
    var draggable7 = SKSpriteNode()
    var draggable8 = SKSpriteNode()
    var draggable9 = SKSpriteNode()
    var draggable10 = SKSpriteNode()
    var sprite = SKSpriteNode()
    
    var bgcomplete = SKSpriteNode()
    var award = SKSpriteNode()
    var nextbutton = SKSpriteNode()
    var btnTryAgain = SKSpriteNode()


    var selectNode = SKSpriteNode()

    let imageNames = ["corn0",
                      "corn1",
                      "corn2",
                      "corn3",
                      "corn4",
                      "corn5",
                      "corn6",
                      "corn7",
                      "corn8",
                      "corn9",
                      "corn10"]
    
    var audionames = ["Zero",
                      "One",
                      "Two",
                      "Three",
                      "Four",
                      "Five",
                      "Six",
                      "Seven",
                      "Eight",
                      "Nine",
                      "Ten",
                      "Eleven",
                      "Twelve",
                      "Thirteen",
                      "Fourteen",
                      "Fifteen",
                      "Sixteen",
                      "Seventeen",
                      "Eighteen",
                      "Nineteen",
                      "Twenty"]

    override func didMove(to view: SKView) {
        self.basket1 = self.childNode(withName: "basket1") as! SKSpriteNode
        self.basket2 = self.childNode(withName: "basket2") as! SKSpriteNode
        self.basket3 = self.childNode(withName: "basket3") as! SKSpriteNode
        self.basket4 = self.childNode(withName: "basket4") as! SKSpriteNode
        self.basket5 = self.childNode(withName: "basket5") as! SKSpriteNode
        self.basket6 = self.childNode(withName: "basket6") as! SKSpriteNode
        self.basket7 = self.childNode(withName: "basket7") as! SKSpriteNode
        self.basket8 = self.childNode(withName: "basket8") as! SKSpriteNode
        self.basket9 = self.childNode(withName: "basket9") as! SKSpriteNode
        self.basket10 = self.childNode(withName: "basket10") as! SKSpriteNode
        self.bgcomplete = self.childNode(withName: "bgcomplete") as! SKSpriteNode
        self.award = self.childNode(withName: "award") as! SKSpriteNode
        self.nextbutton = self.childNode(withName: "nextbutton") as! SKSpriteNode
        self.btnTryAgain = self.childNode(withName: "btnTryAgain") as! SKSpriteNode

        numberLabel = self.childNode(withName: "numberLabel") as! SKLabelNode
        placeoneLabel = self.childNode(withName: "placeoneLabel") as! SKLabelNode
        placetwoLabel = self.childNode(withName: "placetwoLabel") as! SKLabelNode
        
        numberLabel.text = "\(activecorn)"
        placeoneLabel.text = "\(placeone)"
        placetwoLabel.text = "\(placetwo)"
        activemove = true
        
        placeoneLabel.isHidden = true
        placetwoLabel.isHidden = true
        nextbutton.isHidden = true
        bgcomplete.isHidden = true
        award.isHidden = true
        btnTryAgain.isHidden = true
        
        setupcorn()
    
    }
    
    func setupcorn() {
        for i in 0..<imageNames.count {
            
            let imageName = imageNames[i]
            let sprite = SKSpriteNode(imageNamed: imageName)
            let offsetFraction = (CGFloat(i) + 1.0)/(CGFloat(imageNames.count) + 1.0)
            sprite.position = CGPoint(x: size.width * offsetFraction, y: size.height / 1.6)
            sprite.zPosition = 5
            sprite.name = "draggable\(i)"
            addChild(sprite)
            print("draggable\(i)")
            
        }
    }
    
    func checkscore() {
        if 8 == placeone + placetwo {
            
            basket0.removeFromParent()
            basket1.removeFromParent()
            basket2.removeFromParent()
            basket3.removeFromParent()
            basket4.removeFromParent()
            basket5.removeFromParent()
            basket6.removeFromParent()
            basket7.removeFromParent()
            basket8.removeFromParent()
            basket9.removeFromParent()
            basket10.removeFromParent()

            print(placeone)
            print(placetwo)
            print("YOU WIN!!!")
            complete()
        }
            
        else {
            btnTryAgain.isHidden = false
            bgcomplete.isHidden = false
            bgcomplete.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
            bgcomplete.size = CGSize(width: self.size.width, height: self.size.height)
            btnTryAgain.position = CGPoint(x: self.size.width/2, y: self.size.height/4.3)


            basket0.removeFromParent()
            basket1.removeFromParent()
            basket2.removeFromParent()
            basket3.removeFromParent()
            basket4.removeFromParent()
            basket5.removeFromParent()
            basket6.removeFromParent()
            basket7.removeFromParent()
            basket8.removeFromParent()
            basket9.removeFromParent()
            basket10.removeFromParent()

        }
    }
    
    func reset() {
        let meetScene = MeetTsitha(fileNamed: "AddingTo8a")
        let transition = SKTransition.fade(withDuration: 0.5)
        meetScene?.scaleMode = .aspectFill
        scene?.view?.presentScene(meetScene!, transition: transition)
            }
    
    func complete() {
        bgcomplete.run(SKAction.sequence([SKAction.wait(forDuration: 1.5),
                                          SKAction.unhide(),
                                             SKAction.fadeIn(withDuration: 1.5)]))
        award.run(SKAction.sequence([SKAction.wait(forDuration: 1.5),
                                             SKAction.unhide(),
                                             SKAction.fadeIn(withDuration: 1.5)]))
        nextbutton.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                          SKAction.unhide(),
                                             SKAction.fadeIn(withDuration: 2.5)]))
        
        bgcomplete.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        bgcomplete.size = CGSize(width: self.size.width, height: self.size.height)
        award.position = CGPoint(x: self.size.width/2, y: self.size.height/1.6)
        nextbutton.position = CGPoint(x: self.size.width/2, y: self.size.height/4.3)
        playChime()
    }
    
    func fullcomplete() {
        print("You Win!!!!!")
        
    }
    
    func playSound() {
        let otherWait = SKAction.wait(forDuration: 1)
        let runsound = SKAction.playSoundFileNamed("\(String(audionames[(activecorn)])).mp3", waitForCompletion: true)
        let otherSequence = SKAction.sequence([otherWait, runsound])
        run(otherSequence)
    }
    
    func playChime() {
        let otherWait = SKAction.wait(forDuration: 1)
        let runsound = SKAction.playSoundFileNamed("chime.mp3", waitForCompletion: true)
        let otherSequence = SKAction.sequence([otherWait, runsound])
        run(otherSequence)
        
    }
    
    func touchDown(atPoint pos : CGPoint) {
    }
    
    func touchMoved(toPoint pos : CGPoint) {
    }
    
    func touchUp(atPoint pos : CGPoint) {
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesarray = nodes(at: location)
            
            for node in nodesarray {
                
                if node.name == "corn\(activecorn)" {
                    
                }
                
                if node.name == "nextbutton" {
                     let meetScene = MeetTsitha(fileNamed: "AddingTo7a")
                     let transition = SKTransition.fade(withDuration: 0.5)
                     //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                     meetScene?.scaleMode = .aspectFill
                     scene?.view?.presentScene(meetScene!, transition: transition)
                 }

                if node.name == "btnTryAgain" {
                    reset()
                 }
                
            }
            
            if let touch = touches.first {
                let location = touch.location(in: self)
                let touchedNodes = self.nodes(at: location)
                for node in touchedNodes.reversed() {
                    
                    for i in 0..<imageNames.count {
                        
                        if node.name == "draggable\(i)" {
                            self.currentNode = node
                            activecorn = (i)
                            numberLabel.text = "\(activecorn)"
                            playSound()
                        }
                    }

                }
            }
            
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let node = currentNode {
            let touchLocation = touch.location(in: self)
            node.position = touchLocation
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
        
    guard let current = currentNode?.frame else { return }
        
        if activegame == true {
            
        if basket8.frame.intersects(current){
            if activemove == true {
                placeone = activecorn
                currentNode?.removeFromParent()
                placeoneLabel.text = "\(placeone)"
                playSound()
                activemove = false
                placeoneLabel.isHidden = false

                for sprite in self.children{
                    if sprite.name == "draggable0"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable1"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable2"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable3"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable4"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable5"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable6"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable7"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable8"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable9"{
                        sprite.removeFromParent()
                    }
                    if sprite.name == "draggable10"{
                        sprite.removeFromParent()
                    }
                }
                
                setupcorn()

                print("if statement 1 complete")
            }
            
            else {
                placetwo = activecorn
                currentNode?.removeFromParent()
                placetwoLabel.text = "\(placetwo)"
                placetwoLabel.isHidden = false
                activemove = true
                print("statement 2 finnished")
                activegame = false
                checkscore()
            }
        }

    }
        else {
            self.currentNode = nil
        }
    }
    
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.currentNode = nil
    }
    
    
}
