import UIKit
import SpriteKit
import AVFoundation

class MeetTsitha: SKScene {
    var menuActive = true
    
//    var player:AVPlayer!
//    var VideoSprite:SKVideoNode!
    
    var mohawkLabel1:SKLabelNode!
    var mohawkLabel2:SKLabelNode!
    var englishLabel1 = SKLabelNode()
    var englishLabel2 = SKLabelNode()
    var englishLabelTitle = SKLabelNode()

    var attributedText:SKLabelNode!
    let menubg = SKSpriteNode(imageNamed: "iconmenubg")
    let btncount = SKSpriteNode(imageNamed: "menucount")
    let btnadd = SKSpriteNode(imageNamed: "menuadd")
    let btnsub = SKSpriteNode(imageNamed: "menusub")
    let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
    let englishbutton1 = SKSpriteNode(imageNamed: "btnenglish2")
    let englishbutton2 = SKSpriteNode(imageNamed: "btnenglish2")
    let englishbutton3 = SKSpriteNode(imageNamed: "btnenglish2")

    let countingbutton = SKSpriteNode(imageNamed: "group_1")
    let additionbutton = SKSpriteNode(imageNamed: "group_2")
    let subtractionbutton = SKSpriteNode(imageNamed: "group_3")
    let iconmenu = SKSpriteNode(imageNamed: "iconmenu")

    
    override func didMove(to view: SKView) {
        
        self.englishLabel1 = self.childNode(withName: "englishLabel1") as! SKLabelNode
        self.englishLabel2 = self.childNode(withName: "englishLabel2") as! SKLabelNode
        self.englishLabelTitle = self.childNode(withName: "englishLabelTitle") as! SKLabelNode

        englishLabel1.isHidden = true
        englishLabel2.isHidden = true
        englishLabelTitle.isHidden = true

        let background = SKSpriteNode(imageNamed: "background2")
        background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        background.size = CGSize(width: self.size.width, height: self.size.height)
        background.zPosition = -5
        addChild(background)
        
//        let farmer = SKSpriteNode(imageNamed: "Tsitha_Design01b")
//        farmer.position = CGPoint(x: self.size.width/2, y: self.size.height/3.38)
//        farmer.zPosition = 1
//        addChild(farmer)

        let basket = SKSpriteNode(imageNamed: "p1basket")
        basket.position = CGPoint(x: self.size.width/10, y: self.size.height/6.5)
        basket.size = CGSize(width: self.size.width/5, height: self.size.height/9)
        basket.zPosition = 1
        addChild(basket)

        let stack = SKSpriteNode(imageNamed: "p1stack")
        stack.position = CGPoint(x: self.size.width/10, y: self.size.height/4)
        stack.zPosition = 0
        addChild(stack)

        // Menu background
        menubg.name = "menubackground"
        menubg.zPosition = -2
        menubg.position = CGPoint(x: self.size.width/5.06, y: self.size.height/1.09)
        menubg.size = CGSize(width: self.size.width/2.78, height: self.size.height/11.4)
        menubg.isHidden = true
        self.addChild(menubg)
        
        
        iconmenu.name = "iconmenu"
        iconmenu.zPosition = -1
        iconmenu.position = CGPoint(x: self.size.width/18, y: self.size.height/1.09)
        iconmenu.size = CGSize(width: 80, height: 80)
        iconmenu.isHidden = false
        self.addChild(iconmenu)
        //         let btnhome = SKSpriteNode(imageNamed: "iconhomebutton")
        
        btnhome.name = "btnhome"
        btnhome.zPosition = -1
        btnhome.position = CGPoint(x: self.size.width/7.8, y: self.size.height/1.09)
        btnhome.size = CGSize(width: 80, height: 80)
        btnhome.isHidden = true
        self.addChild(btnhome)
        
        btncount.name = "btncount"
        btncount.zPosition = -1
        btncount.position = CGPoint(x: self.size.width/5.2, y: self.size.height/1.09)
        btncount.size = CGSize(width: 80, height: 80)
        btncount.isHidden = true
        self.addChild(btncount)
        
        btnadd.name = "btnadd"
        btnadd.zPosition = -1
        btnadd.position = CGPoint(x: self.size.width/3.84, y: self.size.height/1.09)
        btnadd.size = CGSize(width: 105, height: 80)
        btnadd.isHidden = true
        self.addChild(btnadd)
        
        btnsub.name = "btnsub"
        btnsub.zPosition = -1
        btnsub.position = CGPoint(x: self.size.width/3, y: self.size.height/1.09)
        btnsub.size = CGSize(width: 98, height: 80)
        btnsub.isHidden = true
        self.addChild(btnsub)
        // End Menu

    
    // End Menu

//        let videoURL: NSURL = Bundle.main.url(forResource: "meettsithavideo", withExtension: "mp4")! as NSURL
//        VideoSprite = SKVideoNode(url: videoURL as URL)
//        VideoSprite.size = CGSize(width: 114, height: 65)
//        self.addChild(VideoSprite)
//        VideoSprite.play()
//
//        player = AVPlayer(url: videoURL as URL)
//        player?.actionAtItemEnd = .none
//        player?.isMuted = true
//
//        let playerLayer = AVPlayerLayer(player: player)
//        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
//        playerLayer.zPosition = 100
//
//        playerLayer.frame = view.frame
//
//        player?.play()
        
        
//        NotificationCenter.defaultCenter().addObserver(self,
//                                                         selector: #selector(MenuScene.loopVideo),
//                                                         name: AVPlayerItemDidPlayToEndTimeNotification,
//                                                         object: nil)
//
//        view.layer.addSublayer(playerLayer)
        
        
    }
    
    
//
//    func loopVideo() {
//        player?.seek(to: CMTime.zero)
//        player?.play()
//    }

    
//    func playSound() {
//        let otherWait = SKAction.wait(forDuration: 1)
//        let runsound = SKAction.playSoundFileNamed("NarratorLine02.mp3", waitForCompletion: true)
//        let otherSequence = SKAction.sequence([otherWait, runsound])
//        run(otherSequence)
//
//    }
    
    func menuTouched() {
        if menuActive { // If menu is active
            btnsub.isHidden = false
            btnadd.isHidden = false
            btncount.isHidden = false
            btnhome.isHidden = false
            menubg.isHidden = false

            menuActive = false
        }
        else { // If menu is dormant
            btnsub.isHidden = true
            btnadd.isHidden = true
            btncount.isHidden = true
            btnhome.isHidden = true
            menubg.isHidden = true

            menuActive = true
        }
        
    }
    
    

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let nodesarray = nodes(at: location)

            for node in nodesarray {
                
                if node.name == "englishbutton1" {
                    englishLabel1.isHidden = false
                    englishLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                            SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabel1.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                            SKAction.fadeOut(withDuration: 1)]))
                }

                if node.name == "englishbutton2" {
                    englishLabel2.isHidden = false
                    englishLabel2.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                         SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabel2.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                         SKAction.fadeOut(withDuration: 1)]))
                }

                if node.name == "englishbutton3" {
                    englishLabelTitle.isHidden = false
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                         SKAction.fadeIn(withDuration: 0.5)]))
                    englishLabelTitle.run(SKAction.sequence([SKAction.wait(forDuration: 2.5),
                                                         SKAction.fadeOut(withDuration: 1)]))
                }

                
                if node.name == "iconmenu" {
                    menuTouched()
                }
                
                if node.name == "btnhome" {
                    let meetScene = MeetTsitha(fileNamed: "GameScene")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btncount" {
                    let countScene = MeetTsitha(fileNamed: "CountingIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    countScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(countScene!, transition: transition)
                }
                if node.name == "btnadd" {
                    let meetScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "btnsub" {
                    let meetScene = MeetTsitha(fileNamed: "SubtractionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    meetScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(meetScene!, transition: transition)
                }
                if node.name == "countingbutton" {
                    let countScene = MeetTsitha(fileNamed: "CountingIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    countScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(countScene!, transition: transition)
                }
                if node.name == "subtractionbutton" {
                    let countScene = MeetTsitha(fileNamed: "SubtractionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    countScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(countScene!, transition: transition)
                }
                if node.name == "additionbutton" {
                    let countScene = MeetTsitha(fileNamed: "AdditionIntro")
                    let transition = SKTransition.fade(withDuration: 0.5)
                    //                    let transition = SKTransition.flipVertical(withDuration: 1.0)
                    countScene?.scaleMode = .aspectFill
                    scene?.view?.presentScene(countScene!, transition: transition)
                }

            }

        }
    }
    
    
}
